const express = require("express");
const router = express.Router();
const auth = require('./../auth')

const userController = require("./../controllers/userControllers");

//Registration
router.post("/register", (req, res) => {
	userController.register(req.body).then(result => res.send(result));
})
//Login
router.post("/login", (req, res) => {
	userController.login(req.body).then(result => res.send(result))
})
//get all users
router.get("/all", auth.verify, auth.verifyAdmin, (req, res) =>{
	userController.getAllUsers().then( result => res.send(result))
})

//Get  Users details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	console.log(userData)

	userController.getProfile(userData.id).then( result => res.send(result))
})
//Set USER as ADMIN
router.put("/:userId/setAsAdmin", auth.verify, auth.verifyAdmin, (req, res) => {

	userController.setAsAdmin(req.params.userId).then( result=> res.send(result));
})

module.exports = router;