const Product = require('./../models/Product');

module.exports.addProduct = (reqBody) => {

	let newProduct = new Product ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then((result, error)=> {
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.getAllActiveProducts = () => {
	return Product.find({isActive: true}).then(result=>result)
}

module.exports.getSingleProduct = (params) => {
	return Product.findById(params).then(result => result)
}

module.exports.updateProduct = (params, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(params, updatedProduct, {new: true})
	.then((result, error)=>{
		if(error){
			return error
		} else {
			return result
		}
	})
}

module.exports.archiveProduct = (params) => {
	let archivedProduct = {isActive : false}

	return Product.findByIdAndUpdate(params, archivedProduct, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return true
		}
	})
}