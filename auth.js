let jwt = require('jsonwebtoken');
let secret = "EcommerceAPI";

// creates token
module.exports.createAccesToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}
// Verify token
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return res.send({auth: "failed"})
			} else {
				next();
			}
		})
	} else {
		return res.send("You have to log in")
	}
}
//verify if ADMIN
module.exports.verifyAdmin = (req, res, next) => {
	let token = req.headers.authorization

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		let decoded = jwt.decode(token, {complete: true}).payload

		if(decoded.isAdmin == true){
			next()
		} else {
			res.send(`You are not allowed to do this action`)
		}
	}
}
//verify if NON-ADMIN
module.exports.verifyNonAdmin = (req, res, next) => {
	let token = req.headers.authorization

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		let decoded = jwt.decode(token, {complete: true}).payload

		if(decoded.isAdmin == false){
			next()
		} else {
			res.send(`You are not allowed to do this action`)
		}
	}
}
//decode token
module.exports.decode = (token) => {

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.decode(token, {complete: true}).payload
	}
}
