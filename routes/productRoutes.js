const express = require('express');
const router = express.Router();
const auth = require('./../auth');

const productController = require("./../controllers/productControllers");

//create product
router.post("/addProduct", auth.verify, auth.verifyAdmin, (req, res) => {

	productController.addProduct(req.body).then(result => res.send(result))	
})
//Retrieve all active products
router.get("/activeProducts", (req, res)=>{
	productController.getAllActiveProducts().then(result => res.send(result))
})
//Retrieve single products
router.get("/:productId", (req, res)=>{
	productController.getSingleProduct(req.params.productId).then(result=>res.send(result))
})
//Update Product info
router.put("/:productId/update", auth.verify, auth.verifyAdmin, (req, res)=>{
	productController.updateProduct(req.params.productId, req.body ).then(result => res.send(result))
})
//Archive Product
router.put("/:productId/archive", auth.verify, auth.verifyAdmin, (req, res) => {
	productController.archiveProduct(req.params.productId).then(result => res.send(result))
})


module.exports = router;