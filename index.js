const express = require('express');
const mongoose = require('mongoose');
const PORT = process.env.PORT || 3000;
const app = express();
const cors = require('cors');

//routes
let userRoutes = require('./routes/userRoutes');
let productRoutes = require('./routes/productRoutes');
let orderRoutes = require('./routes/orderRoutes')

//middleswares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//mongoose connection
mongoose.connect("mongodb+srv://arthur25:arthurespartinez@cluster0.iymnv.mongodb.net/e-commerce?retryWrites=true&w=majority",
	{useNewUrlParser: true, useUnifiedTopology: true}
).then(console.log(`Connected to Database`))
.catch( (error) => console.log(error))

//routes
app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);
app.use("/api/orders", orderRoutes)

app.listen(PORT, () => console.log(`Server running at port ${PORT}`))