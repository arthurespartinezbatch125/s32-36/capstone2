const User = require("./../models/User");
const bcrypt = require("bcrypt");
const auth = require("./../auth")

module.exports.register = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 8)
	})

	return User.find({email: reqBody.email})
	.then((result) => {
		if(result.length !=0){
			return `Email already exists`
		} else {
			return newUser.save().then( (result, error) => {
				if(error){
					return error
				} else {
					return true
				}
			})
		}
	})
}

module.exports.login = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result)=>{

		// console.log(result)
		if(result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			if(isPasswordCorrect === true){
				return { access: auth.createAccesToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}
module.exports.getProfile = (data) => {
	return User.findById(data).then( result => {

		result.password = "******"
		return result
	})
}
module.exports.getAllUsers = () => {
	return User.find().then(result => result)
}
module.exports.setAsAdmin = (params) => {

	let setUserAdmin = { isAdmin: true}

	return User.findByIdAndUpdate(params, setUserAdmin, {new: true}).
	then((result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}