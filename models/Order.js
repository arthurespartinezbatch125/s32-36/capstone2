const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	productId: {
		type: String,
		required: [true, "Product ID is required"]
	},
	quantity: {
		type: Number,
		required: true,
		min: [1, "Minimum order quantity is 1"]
	},
	totalAmount: {
		type: Number,
		required: true
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})
module.exports = mongoose.model("Order", orderSchema)