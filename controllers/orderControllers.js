const Order = require('./../models/Order');
const Product = require('./../models/Product');
const User = require('./../models/User')

module.exports.createOrder = async (data) => {

	let price = await Product.findById(data.productId).then(result => {

		return result.save().then((result, error) => {
			if(error){
				return false
			} else {
				return result.price
			}
		})
	})

	let newOrder = new Order ({
		userId: data.userId,
		productId: data.productId,
		quantity: data.quantity,
		totalAmount: price*data.quantity
	})
	let updatedOrder = {
		quantity: data.quantity,
		totalAmount: price*data.quantity
	}

	return Order.find({userId: data.userId, productId: data.productId}).then((result)=> {
		if(result.length != 0){

			return Order.findOneAndUpdate({userId: data.userId, productId: data.productId}, updatedOrder)
			.then((result, error)=> {
				if(error){
					return false
				} else {
					return true
				}
			})
		} else {

			return newOrder.save().then((result, error) => {
				if(error){
					return false
				} else {
					return true
				}
			})
		}
	})
}

module.exports.getAllOrders = () => {
	return Order.find().then(result => result)
}

module.exports.getMyOrders = (user) => {
	return Order.find({userId: user}).then(result => result)
}
