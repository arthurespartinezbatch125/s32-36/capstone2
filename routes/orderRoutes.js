const express = require('express');
const router = express.Router();
const auth = require('./../auth');

const orderController = require("./../controllers/orderControllers")

//create order
router.post("/checkout", auth.verify, auth.verifyNonAdmin, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	orderController.createOrder(data).then(result => res.send(result))
})

//retrieves all orders
router.get("/all", auth.verify, auth.verifyAdmin, (req, res) => {
	orderController.getAllOrders().then(result => res.send(result))
})
//retrieves users orders
router.get("/myOrders", auth.verify, auth.verifyNonAdmin, (req,res) => {

	userId = auth.decode(req.headers.authorization).id
	orderController.getMyOrders(userId).then(result => res.send(result))
})
module.exports = router;